<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><sitemesh:write property='title' /></title>
<style>
textarea {
	resize: none;
}

h4 {
	margin: 10px 0px 0px 0px;
}
.error {
	color: #ff0000;
	background-color: #ffEEEE;
	padding: 8px;
	display: block;
	border: 1px solid #ff0000;
}
</style>
<sitemesh:write property='head' />
</head>
<body>
	<h1>League of Legends - news!</h1>
	<ul>
		<c:choose>
			<c:when test="${not empty loggedUser}">
				<li>
					Logged as: ${ loggedUser.username }, <a href="/blogger/j_spring_security_logout">logout</a>
				</li>
			</c:when>

			<c:otherwise>
				<li>
					<a href="/blogger/login">Login</a>
				</li>
				<li>
					<a href="/blogger/register">Rejestracja</a>
				</li>
			</c:otherwise>
		</c:choose> 
		<security:authorize ifAnyGranted="ROLE_USER,ROLE_ADMIN">
			<li><a href="/blogger/user/profile">Profile</a></li>
		</security:authorize>
		<li><a href="/blogger/">Main page</a></li>
		<li><a href="/blogger/article/list">Articles</a></li>
	</ul>
	
	<sitemesh:write property='body' />
</body>
</html>