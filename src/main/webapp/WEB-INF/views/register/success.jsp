<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h2>Success registered</h2>
${ errors }
<table>

	<tr>
		<td>Username</td>
		<td>${ login }</td>
	</tr>
	<tr>
		<td>Email</td>
		<td>${ email }</td>
	</tr>
	<tr>
		<td>Password</td>
		<td>${ password }</td>
	</tr>
</table>
