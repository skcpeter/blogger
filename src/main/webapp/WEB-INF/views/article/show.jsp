<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<h2>${ article.title }</h2>
<h4>Author: ${ article.user.username }</h4>

<p>${ article.content }</p>

<hr />
<c:choose>
	<c:when test="${not empty comments}">
		<h3>Comments: ${fn:length(comments)}</h3>
		<c:forEach var="c" items="${comments}"
			varStatus="status">
			<p>Author: ${ c.user.username }</p>
			<p>${ c.content }</p>
			<security:authorize ifAnyGranted="ROLE_ADMIN">
				<a href="/blogger/comment/delete/${ article.id }/${ c.id }">Delete comment</a>
			</security:authorize>
			<hr />
		</c:forEach>
	</c:when>

	<c:otherwise>
		Article don't have any comments.
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${not empty loggedUser}">
		<form:form method="POST"
			action="/blogger/article/show/comment/${ article.id }"
			modelAttribute="comment">
			<table width="300">
			<tr><td><h4>Add comment</h4></td></tr>
				<tr>
					<td><form:errors path="*" /></td>
				</tr>
				<tr>
					<td><form:textarea path="content" /></td>
				</tr>
				<tr>
					<td><input type="submit" value="Submit" /></td>
				</tr>
			</table>
		</form:form>
	</c:when>

	<c:otherwise>
		Please <a href="/blogger/login">log in</a> for add comment.
	</c:otherwise>
</c:choose>