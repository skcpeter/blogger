<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib prefix="paging" uri="/WEB-INF/paging.tld"%>


<h2>Article list</h2>
<h4><a href="/blogger/article/add">Add article</a></h4>

<c:choose>
	<c:when test="${not empty articles}">

		<form method="POST" action="/blogger/search">
			<table>
				<tr>
					<td><label for="search">Search</label></td>
					<td><input type="text" id="name" name="search" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Submit" /></td>
				</tr>
			</table>
		</form>
		
		<paging:paging pageListHolder="${articles}" offset="4"/>
		
 		<table>
			<thead>
				<tr>
					<th></th>
					<th>Title</th>
					<th>Author</th>
					<security:authorize ifAnyGranted="ROLE_ADMIN">
						<th>Actions</th>
					</security:authorize>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="article" items="${articles.pageList}" varStatus="status">
					<tr>
						<td>${ status.index + 1 }</td>
						<td><a href="/blogger/article/show/${ article.id }">${ article.title }</a></td>
						<td>${ article.user.username }</td>
						<security:authorize ifAnyGranted="ROLE_ADMIN">
							<td>
								<a href="/blogger/article/edit/${ article.id }">Edit</a>
								<a href="/blogger/article/delete/${ article.id }">Delete</a>
							</td>
						</security:authorize>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<paging:paging pageListHolder="${articles}" offset="4"/>
	</c:when>

	<c:otherwise>
		You don't have any articles
	</c:otherwise>
</c:choose>
