<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h2>Article form</h2>
<form:form method="POST" modelAttribute="article">
	<table>
		<tr>
			<td><form:label path="title">Title</form:label></td>
			<td><form:input path="title" /></td>
			<td><form:errors path="title"  cssClass="error"/></td>
		</tr>
		<tr>
			<td><form:label path="content">Content</form:label></td>
			<td><form:textarea path="content" rows="5" cols="30"/></td>
			<td><form:errors path="content" cssClass="error"/></td>
		</tr>
		
		<tr>
			<td colspan="2"><input type="submit" value="Submit" /></td>
		</tr>
	</table>
</form:form>
