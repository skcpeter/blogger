<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>


<h2>Search result for "${ search }"</h2>

<c:choose>
	<c:when test="${not empty articles}">
		<table>
			<thead>
				<tr>
					<th></th>
					<th>Title</th>
					<th>Author</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="article" items="${articles}" varStatus="status">
					<tr>
						<td>${ status.index + 1 }</td>
						<td><a href="/blogger/article/show/${ article.id }">${ article.title }</a></td>
						<td>${ article.user.username }</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:when>

	<c:otherwise>
		No result
	</c:otherwise>
</c:choose>
