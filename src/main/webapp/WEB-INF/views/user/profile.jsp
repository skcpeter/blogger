<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<h2>Profile form</h2>
<form:form method="POST" modelAttribute="user">
	<form:hidden path="username" />
	<form:hidden path="id" />

	<table>
		<tr>
			<td colspan="2">
				${ message }
				<form:errors path="*" cssClass="error"/>
			</td>
		</tr>
		<tr>
		
			<td><form:label path="username">Username</form:label></td>
			<td>${ user.username }</td>
		</tr>
		<tr>
			<td><form:label path="email">Email</form:label></td>
			<td><form:input path="email" /></td>
		</tr>
		<tr>
			<td><form:label path="password">Password</form:label></td>
			<td><form:password path="password" /></td>
		</tr>
		<tr>
			<td><form:label path="confirmPassword">Confirm password</form:label></td>
			<td><form:password path="confirmPassword" /></td>
		</tr>
		
		<tr>
			<td colspan="2"><input type="submit" value="Submit" /></td>
		</tr>
	</table>
</form:form>
