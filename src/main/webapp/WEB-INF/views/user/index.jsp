<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Blogger</title>
</head>
<body>
    <h1>/user</h1>
    
    <h3>Message : ${message}</h3>	
	<h3>Username : ${loggedUser.username}</h3>	
 
	<a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
</body>
</html>