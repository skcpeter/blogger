package pl.psalkowski.blogger.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.blogger.model.Comment;

@Repository(value="commentDao")
@Transactional(readOnly = true)
public class CommentDao extends GenericDao<Comment>{

	public CommentDao() {
		setClazz(Comment.class);
	}
}
