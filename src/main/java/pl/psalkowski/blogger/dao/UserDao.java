package pl.psalkowski.blogger.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.blogger.model.User;

@Repository(value="userDao")
@Transactional(readOnly = true)
public class UserDao extends GenericDao<User>{
	
	public UserDao() {
		setClazz(User.class);
	}
	
	public boolean isUsernameAvailable(String username) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		Criterion name = Restrictions.like("username", username).ignoreCase();
		criteria.add(name);
		
		List result = criteria.list();
		return result.isEmpty();
	}
	
	public boolean isEmailAvailable(String email) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.like("email", email).ignoreCase());
		
		List result = criteria.list();
		return result.isEmpty();
	}

	public User findByUsername(String username) {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.like("username", username).ignoreCase());
		
		return (User) criteria.uniqueResult();
	}
}