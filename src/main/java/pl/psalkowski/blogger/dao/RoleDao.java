package pl.psalkowski.blogger.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.blogger.model.Role;
import pl.psalkowski.blogger.model.User;

@Repository(value="roleDao")
@Transactional(readOnly = true)
public class RoleDao extends GenericDao<Role> {
	
	public RoleDao() {
		setClazz(Role.class);
	}
	
	@Transactional(readOnly = false)
	public Role findByAuthority(String authority) {
		Criteria criteria = getCurrentSession().createCriteria(Role.class);
		criteria.add(Restrictions.eq("authority", authority));
		
		return (Role) criteria.uniqueResult();
	}
}