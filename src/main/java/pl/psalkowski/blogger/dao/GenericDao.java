package pl.psalkowski.blogger.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.blogger.model.GenericEntity;


@Repository
public abstract class GenericDao<T extends GenericEntity> {

	private Class<T> clazz;

	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
	    this.sessionFactory = sessionFactory;
	}
	
	public final void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional
	public T find(long id) {
		return (T) getCurrentSession().get(clazz, id);
	}
	
	@Transactional(readOnly = false)
	public void save(T entity) {
		getCurrentSession().save(entity);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<T> findAll() {
		return getCurrentSession().createQuery("from " + clazz.getName())
				.list();
	}

	@Transactional(readOnly = false)
	public void create(T entity) {
		getCurrentSession().persist(entity);
	}
	
	@Transactional(readOnly = false)
	public T merge(T entity) {
		return (T) getCurrentSession().merge(entity);
	}
	
	@Transactional(readOnly = false)
	public void refresh(T entity) {
		getCurrentSession().refresh(entity);
	}

	@Transactional(readOnly = false)
	public void delete(T entity) {
		getCurrentSession().delete(entity);
	}

	@Transactional(readOnly = false)
	public void deleteById(long entityId) {
		T entity = find(entityId);
		delete(entity);
	}

	protected final Session getCurrentSession() {
		try {
			return sessionFactory.getCurrentSession();
		} catch(HibernateException ex) {
			return sessionFactory.openSession();
		}
	}
}