package pl.psalkowski.blogger.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pl.psalkowski.blogger.model.Article;
import pl.psalkowski.blogger.model.User;

@Repository(value="articleDao")
@Transactional(readOnly = true)
public class ArticleDao extends GenericDao<Article>{

	public ArticleDao() {
		setClazz(Article.class);
	}

	public List<Article> search(String search) {
		Criteria criteria = getCurrentSession().createCriteria(Article.class);
		criteria.add(Restrictions.like("title", "%"+search+"%").ignoreCase());
		return criteria.list();
	}
}
