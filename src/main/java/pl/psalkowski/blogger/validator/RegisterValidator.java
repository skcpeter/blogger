package pl.psalkowski.blogger.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.psalkowski.blogger.model.User;

public class RegisterValidator implements Validator {
	
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Override
	public boolean supports(Class arg0) {
		return User.class.equals(arg0);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ValidationUtils.rejectIfEmpty(e, "username", "user.username.empty");
		ValidationUtils.rejectIfEmpty(e, "email", "user.email.empty");
		ValidationUtils.rejectIfEmpty(e, "password", "user.password.empty");

		User user = (User) obj;
		if (!user.getPassword().equals(user.getConfirmPassword())) {
			e.rejectValue("confirmPassword", "user.confirmPassword.match");
		}
		
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(user.getEmail());
		if(!matcher.matches()) {
			e.rejectValue("email", "user.email.invalid");
		}
	}
}
