package pl.psalkowski.blogger.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.psalkowski.blogger.model.Article;
import pl.psalkowski.blogger.model.Comment;
import pl.psalkowski.blogger.service.ArticleService;

@Controller
@RequestMapping(value = "/article")
public class ArticleController extends GenericController {

	private static final Logger logger = LoggerFactory
			.getLogger(ArticleController.class);

	@Autowired
	private ArticleService articleService;

	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String show(@PathVariable Integer id, Model model) {
		Article article = articleService.find(id);
		model.addAttribute("article", article);
		model.addAttribute("comments", article.getComments());
		model.addAttribute("comment", new Comment());

		logger.info(articleService.find(id).getComments().toString());
		return "article/show";
	}

	@RequestMapping(value = "/show/comment/{id}", method = RequestMethod.POST)
	public String comment(@Valid @ModelAttribute("comment") Comment comment,
			@PathVariable Integer id, BindingResult errors, Model model,
			Principal principal) {

		if (errors.hasErrors()) {
			return "article/show";
		}

		articleService.addComment(id, comment, principal);
		return "redirect:/article/show/" + id;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(HttpServletRequest request, Model model) {

		PagedListHolder<Article> articles = (PagedListHolder<Article>) request.getSession().getAttribute("articlesPagination");
		String page = request.getParameter("page");

		if (page == null) {
			articles = new PagedListHolder<Article>(articleService.findAll());
			request.getSession().setAttribute("articlesPagination", articles);
			articles.setPageSize(1);
		} else {
			if (page.equalsIgnoreCase("next")) {
				articles.nextPage();
			} else if (page.equalsIgnoreCase("prev")) {
				articles.previousPage();
			} else if (page.equalsIgnoreCase("first")) {
				articles.setPage(0);
			} else if (page.equalsIgnoreCase("last")) {
				articles.setPage(articles.getPageCount());
			} else {
				articles.setPage(Integer.parseInt(page) - 1);
			}
		}
		model.addAttribute("articles", articles);

		return "article/list";
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String form(Model model) {

		model.addAttribute("article", new Article());
		return "article/form";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addProcess(@Valid @ModelAttribute("article") Article article,
			BindingResult errors, ModelMap model, Principal principal) {

		if (errors.hasErrors()) {
			return "article/form";
		}

		articleService.create(article, principal);

		return "redirect:/article/list";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable Integer id, Model model) {
		Article artice = articleService.find(id);
		model.addAttribute("article", artice);

		logger.info(artice.toString());

		return "article/form";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editProcess(@Valid @ModelAttribute Article article,
			BindingResult errors, ModelMap model) {

		if (errors.hasErrors()) {
			return "article/form";
		}
		logger.info(article.toString());
		articleService.update(article);
		logger.info(article.toString());

		return "redirect:/article/list";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable Integer id, Model model) {
		articleService.delete(id);

		return "redirect:/article/list";
	}
}
