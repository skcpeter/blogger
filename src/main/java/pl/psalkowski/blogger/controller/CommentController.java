package pl.psalkowski.blogger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.psalkowski.blogger.model.Comment;
import pl.psalkowski.blogger.service.CommentService;

@Controller
@RequestMapping(value = "/comment")
public class CommentController extends GenericController {
	
	@Autowired
	private CommentService commentService;
	
	@RequestMapping(value = "/delete/{article}/{comment}")
	public String delete(@PathVariable("article") Integer article, @PathVariable("comment") Integer comment) {
		commentService.delete(comment);
		return "redirect:/article/show/" + article;
	}

}
