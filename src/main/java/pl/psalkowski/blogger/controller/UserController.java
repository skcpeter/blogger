package pl.psalkowski.blogger.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.psalkowski.blogger.model.User;
import pl.psalkowski.blogger.service.UserService;
import pl.psalkowski.blogger.validator.ProfileValidator;

@Controller
@RequestMapping(value = "/user")
public class UserController extends GenericController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@InitBinder("user")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new ProfileValidator());
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "user/index";
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profile(Model model, Principal principal) {
		User user = userService.findByUsername(principal.getName());
		
		model.addAttribute("user", user);
		return "user/profile";
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public String update(@Valid @ModelAttribute("user") User user, BindingResult errors, RedirectAttributes redirectAttributes) {
		
		if(errors.hasErrors()) {
			return "user/profile";
		}
		
		userService.update(user);
		redirectAttributes.addFlashAttribute("message", "Profile has been updated successfully");
		
		return "redirect:/user/profile";
	}
}
