package pl.psalkowski.blogger.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import pl.psalkowski.blogger.dao.ArticleDao;
import pl.psalkowski.blogger.model.Article;

@Controller
public class SearchController extends GenericController {

	@Autowired
	private ArticleDao articleDao;
	
	@RequestMapping("/search")
	public String search(@RequestParam("search") String search, Model model) {
		
		List<Article> articles = articleDao.search(search);
		
		model.addAttribute("articles", articles);
		model.addAttribute("search", search);
		return "search";
	}
}
