package pl.psalkowski.blogger.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.psalkowski.blogger.model.User;
import pl.psalkowski.blogger.service.UserService;
import pl.psalkowski.blogger.validator.RegisterValidator;

@Controller
public class RegisterController extends GenericController {

	@Autowired
	private UserService userService;
	
	
	@InitBinder("user")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new RegisterValidator());
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model) {

		model.addAttribute("user", new User());
		return "register/form";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String addStudent(@Valid @ModelAttribute("user") User user,
			 BindingResult errors, ModelMap model) {

		if (!userService.isValid(user, errors)) {
			return "register/form";
		}
		
		userService.create(user);
		
		return "redirect:/";
	}
}
