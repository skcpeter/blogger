package pl.psalkowski.blogger.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;


@Controller
public class GenericController {
	
	@ModelAttribute("loggedUser")
	public User getCurrentUser(){
		if(SecurityContextHolder.getContext().getAuthentication() == null)
			return null;
		
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof String)
			return null;
		
	    return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
}
