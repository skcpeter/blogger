package pl.psalkowski.blogger.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.psalkowski.blogger.dao.ArticleDao;
import pl.psalkowski.blogger.dao.CommentDao;
import pl.psalkowski.blogger.model.Article;
import pl.psalkowski.blogger.model.Comment;

@Service("commentService")
public class CommentService {

	
	@Autowired
	private ArticleDao articleDao;
	
	@Autowired
	private CommentDao commentDao;
	
	
	public void delete(Comment comment) {
		//Article article = articleDao.find(comment.getArticle().getId());
		//article.getComments().remove(comment);
		
		commentDao.delete(comment);
		//articleDao.save(article);
	}
	
	public void delete(int commentId) {
		delete(commentDao.find(commentId));
	}
	
}
