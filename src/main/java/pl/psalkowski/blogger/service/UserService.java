package pl.psalkowski.blogger.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import pl.psalkowski.blogger.dao.RoleDao;
import pl.psalkowski.blogger.dao.UserDao;
import pl.psalkowski.blogger.model.Role;
import pl.psalkowski.blogger.model.User;

@Service(value = "userService")
public class UserService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;

	@SuppressWarnings("restriction")
	private String hashPassword(String password) {
		ShaPasswordEncoder crypter = new ShaPasswordEncoder(256);
		return crypter.encodePassword(password, null);
	}
	
	public boolean isValid(User user,  BindingResult errors) {
		if(!userDao.isEmailAvailable(user.getEmail())) {
			errors.reject("email", "Adres email jest w u�yciu");
		}
		if(!userDao.isUsernameAvailable(user.getUsername())) {
			errors.reject("username", "Login jest w u�yciu");
		}
		
		return !errors.hasErrors();
	}

	public void create(User user) {
		user.setEnabled(true);
		user.setRole(roleDao.findByAuthority("ROLE_USER"));
		user.setPassword(hashPassword(user.getPassword()));
		userDao.merge(user);
	}
	
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	public void update(User user) {
		User original = userDao.find(user.getId());
		original.setEmail(user.getEmail());
		
		if(user.getPassword().trim().length() > 0 && !user.getPassword().equals(original.getPassword()))
			original.setPassword(hashPassword(user.getPassword()));
		
		userDao.merge(original);
	}
}
