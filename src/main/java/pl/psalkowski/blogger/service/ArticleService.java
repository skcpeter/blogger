package pl.psalkowski.blogger.service;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.psalkowski.blogger.dao.ArticleDao;
import pl.psalkowski.blogger.dao.CommentDao;
import pl.psalkowski.blogger.dao.UserDao;
import pl.psalkowski.blogger.model.Article;
import pl.psalkowski.blogger.model.Comment;

@Service("articleService")
public class ArticleService {

	@Autowired
	private ArticleDao articleDao;
	
	@Autowired
	private CommentDao commentDao;
	
	@Autowired
	private UserDao userDao;
	
	public void create(Article article, Principal principal) {
		article.setUser(userDao.findByUsername(principal.getName()));
		articleDao.merge(article);
	}

	public List<Article> findAll() {
		return articleDao.findAll();
	}

	public Article find(Integer id) {
		return articleDao.find(id);
	}

	public Article update(Article form) {
		Article article = articleDao.find(form.getId());
		article.setTitle(form.getTitle());
		article.setContent(form.getContent());
		return articleDao.merge(article);
	}
	
	public Article merge(Article article) {
		return articleDao.merge(article);
	}

	public void delete(Integer id) {
		articleDao.deleteById(id);
	}

	public void addComment(Integer id, Comment comment, Principal principal) {
		Article article = articleDao.find(id);
		comment.setUser(userDao.findByUsername(principal.getName()));
		article.addComment(comment);
		articleDao.merge(article);
	}
}
