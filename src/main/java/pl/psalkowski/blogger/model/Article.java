package pl.psalkowski.blogger.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "articles")
public class Article implements GenericEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8827346288475901772L;
	
	@Id
	@GeneratedValue
	private long id;
	
	@Length(min=3)
	private String title;
	
	@Length(min=3)
	private String content;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade= { CascadeType.MERGE })
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="article", cascade= { CascadeType.ALL })
	private List<Comment> comments;
	
	
	public Article() {
		this.comments = new ArrayList<Comment>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void addComment(Comment comment) {
		comment.setArticle(this);
		this.comments.add(comment);
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}
	
	
}
