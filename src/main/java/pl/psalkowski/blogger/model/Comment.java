package pl.psalkowski.blogger.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "comments")
public class Comment implements GenericEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1882519154197764821L;
	
	@Id
	@GeneratedValue
	private long id;
	private String content;
	
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name="user_id", nullable=false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name="article_id", nullable=false)
	private Article article;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}
	
}
