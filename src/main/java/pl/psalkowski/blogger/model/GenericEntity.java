package pl.psalkowski.blogger.model;

import java.io.Serializable;

public interface GenericEntity extends Serializable {
	public long getId();
}
